import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@mui/material/TextField';
import { useState} from 'react';
import Link from 'next/link'

export default function signUp(){

   
    const [name,setName] = useState("")
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")

    return (
        <Box sx={{width: '100%',height: 602,background: 'linear-gradient(to right, #8360c3, #2ebf91)'}}>
            <Grid  container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
            <Grid item xs={3}>
            <Card sx={{ maxWidth: 500}}>
                <CardContent >
                    <Typography gutterBottom variant="h4" component="div" textAlign="center">
                                    Sign Up
                    </Typography>
                <br/>
                    <TextField
                id="name"
                label="Enter Name"
                type="text"
                value={name}
                autoComplete="current-name" required onChange={(e)=>setName(e.target.value)}
                /><br/> <br/>
                    <TextField
                id="emailId"
                label="Enter Email"
                type="email" value={email}
                autoComplete="current-email" required onChange={(e)=>setEmail(e.target.value)}
                />
                <br/>
                <br/>
                    <TextField
                id="password"
                label="Enter Password" value={password}
                type="password" required onChange={(e)=>setPassword(e.target.value)} 
                />
                <br/><br/>
                    <TextField
                id="password-repeat" label="Enter Password again"
                type="password" required />
                <br/><br/>
                <Link href="/login">
                    <a>Already have an account?</a>
                </Link>
                </CardContent>
                <CardActions sx={{ justifyContent: "center" }}>
                    <Button size="large" variant="contained">Sign Up</Button>
                </CardActions>
            </Card>
            </Grid>      
            </Grid> 
    
        </Box>)
    

    
}