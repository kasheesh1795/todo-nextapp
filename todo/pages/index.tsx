import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@material-ui/core/Grid';
import { useRouter } from 'next/router'
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export default function Home() {

  const router = useRouter()

  function handleClick(){
    
      router.push('/signup')
  }

    return(
      <Box
          sx={{
            width: '100%',
            height: 602,
            background: 'linear-gradient(to right, #8360c3, #2ebf91)'
            }
          }>
          <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" color="primary">
              <Toolbar>
                <IconButton
                  size="large"
                  edge="start"
                  color="inherit"
                  aria-label="menu"
                  sx={{ mr: 2 }}
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" justifyContent="center" component="div" sx={{ flexGrow: 1 }}>
                  TO DO APP
                </Typography>
                <Button color="inherit" onClick={()=>router.push('/login')}>Login</Button>
                <Button color="inherit" onClick={()=>router.push('/signup')}>Sign Up</Button>
              </Toolbar>
            </AppBar>
          </Box>

        <Grid  container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
        <Grid item xs={3}>
        <Card sx={{ maxWidth: 500}}>
            <CardMedia
              component="img"
              alt="todo_list"
              height="autoHeight"
              src="https://picsum.photos/400/300">
            </CardMedia>
            <CardContent >
              <Typography gutterBottom variant="h4" component="div" textAlign="center">
                Get Started
              </Typography>
            </CardContent>
            <CardActions sx={{ justifyContent: "center" }}>
              <Button size="large" variant="contained"onClick={handleClick}>Let's Go!</Button>
            </CardActions>
          </Card>
        </Grid>      
        </Grid> 

    </Box>)
  

}

