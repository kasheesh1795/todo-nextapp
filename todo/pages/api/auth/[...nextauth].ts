import NextAuth from "next-auth/next";
import CredentialsProvider  from "next-auth/providers/credentials";
import Email from "next-auth/providers/email";
import EmailProvider from "next-auth/providers/email";
import GoogleProvider from "next-auth/providers/google";
import GitHubProvider from "next-auth/providers/github";



export default NextAuth({
    providers:[GitHubProvider({
        clientId: process.env.GITHUB_CLIENT_ID,
        clientSecret: process.env.GITHUB_CLIENT_SECRET
      }),EmailProvider({
        server: process.env.EMAIL_SERVER,
        from: process.env.EMAIL_FROM
      }), CredentialsProvider(
        {
            name:"credentials",
            credentials:{
                username: { label: "Username" , type:"text",placeholder:"johndoe@test.com"},
                password: { label: "Password" , type:"password"}
            },
            authorize: (credentials) => {
                if(credentials.username==="john" && credentials.password==="test")
                {
                    return {
                        id: 2,
                        name: "John",
                        email: "johndoes@test.com"
                    }
                }

                return null
            }
        }
    )
    ],
    callbacks: {
        jwt: ({token , user}) => {
            if(user){
                token.id = user.id
            }

            return token
        },
        session: ({session,token}) => {
            if(token){
                session.id = token.id
            }

            return session
        },
    },
    secret:"test",
    jwt:{
        secret:"test",
        encryption:true,
    },
    pages:{
        signIn: "../../../signup"
    }
})