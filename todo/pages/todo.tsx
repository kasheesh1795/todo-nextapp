import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Input from '@mui/material/Input';
import Grid from '@material-ui/core/Grid';


const ariaLabel = { 'aria-label': 'description' };


export default function toDo(){
    
    return (
      <Box
      sx={{
        width: '100%',
        height: 602,
        background: 'linear-gradient(to right, #8360c3, #2ebf91)'
        }
      }>
      <Grid  container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
      <Grid item xs={3}>
        <TextField sx={{width:'100%',background:'white',borderRadius:'10px'}} variant="filled" id="fullWidth"/>
      </Grid>
      </Grid>
      </Box>
    )
}
